import uuid

from django.db import models


class UuidPrimaryKey(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)

    class Meta:
        abstract = True
