# salala-backend

Salala has two repos for separating concerns. The [front-end](https://gitlab.com/urshala/salala-frontend) uses React. The backend uses Django Rest Framework with postgres database. The python version for the project is 3.6.0.

## How to get the project
1. Add your public ssh_key to gitlab. Info can be found [here](https://docs.gitlab.com/ee/ssh/)
2. `git clone https://gitlab.com/urshala/salala-backend` You can fork too if you want but won't suggest it.

## How to run the project
You can use docker to run the project or configure it locally. Docker should be the way to go.

### Running user docker
1. Install docker
2. Create an `.env` file. Project already have `.env.template` file you can copy the values from there
to your `.env` file
3. Execute `docker-compose up` . This command will apply the migrations and runs the django project.

### Running locally
1. Create python virtual environement
`python -m venv /path/to/new/virtual/environment`
`E.g. python -m venv salala_venv`

2. Activate the virtual environement
`source name_of_env/bin/activate`
`E.g. source salala_venv/bin/activate`

3. Install the dependencies
`pip install -r requirements_localdev.txt`

4. The project uses pre-commit hook so that all the codes will be formatted by
black before you can commit them if you miss something.
`pre-commit install`


## Coding style
2. Project uses `black` for code formatting and `flake8` code style guide

## Merge/Pull requests
1. Let's try to review each other's code as much as possible
2. Do not push to master branch directly. This branch should contain the deployable code all the time
2. If you are working on some feature create your own branch, work on it and create merge/pull request
   against the master branch.


