from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import UuidPrimaryKey


class ServiceCategory(UuidPrimaryKey):
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(_("Service category"), max_length=40, null=False)

    def __str__(self):
        parent_name = self.parent.name if self.parent else ""
        return f"{parent_name} - {self.name}"


class Service(UuidPrimaryKey):
    category = models.ForeignKey(
        ServiceCategory, on_delete=models.CASCADE, null=True, related_name="services"
    )
    provider = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="service_providers",
    )
    rate = models.DecimalField(_("Rate"), max_digits=9, decimal_places=2)

    def __str__(self):
        return f"{self.provider.first_name} - {self.category.name}"
