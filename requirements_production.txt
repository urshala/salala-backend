Django==3.1.1
djangorestframework==3.11.0
django-environ==0.4.5
psycopg2-binary==2.8.6
djangorestframework-simplejwt==4.4.0
django-cors-headers==3.5.0
