#!/bin/sh

if [ "$1" != "production" -a "$1" != "development" ]; then
    "$@"
    exit $?
fi

# wait for database to be ready
while ! nc -z ${DB_HOST} ${DB_PORT};
    do sleep 1;
    done;
echo "Database seems to be ready."

echo "Apply database migrations"
./manage.py migrate --noinput

if [ "$1" = "production" ]; then
    # TODO: Run server in production
    echo "Running production server"
else
    echo "Running development server"
    exec ./manage.py runserver 0.0.0.0:${API_PORT}
fi
