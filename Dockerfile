############ Base Image ##################
FROM python:3.6-slim as base_image

ENV PYTHONUNBUFFERED 1

WORKDIR /app

ADD requirements_localdev.txt ./requirements_localdev.txt
ADD requirements_production.txt ./requirements_production.txt

RUN apt-get update \
    && apt-get install netcat -y \
    && pip install --no-cache-dir -r requirements_production.txt


COPY . .

ENTRYPOINT [ "./django-entrypoint.sh" ]

############ Production environment ##################
FROM base_image as production_image

CMD [ "production" ]

############  Development environment ################
FROM production_image as development_image

RUN pip install --no-cache-dir -r requirements_localdev.txt

CMD [ "development" ]
