from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_verification_email(user, data):
    plain_msg = render_to_string("user_verification_email.txt", data)
    html_msg = render_to_string("user_verification_email.html", data)
    salala_email = "salala@gmail.com"

    send_mail(
        "Salala account verification",
        plain_msg,
        salala_email,
        [user.email],
        html_message=html_msg,
    )
