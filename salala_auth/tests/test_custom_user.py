import pytest
from django.contrib.auth import get_user_model

User = get_user_model()


@pytest.mark.django_db
def test_create_normal_user():
    email = "firstname.lastname@mail.com"
    password = "password"
    assert User.objects.count() == 0

    user = User.objects.create_user(email=email, password=password)

    assert User.objects.count() == 1
    assert user.email == email
    assert user.is_superuser == False


@pytest.mark.django_db
def test_create_super_user():
    email = "firstname.lastname@mail.com"
    password = "password"
    assert User.objects.count() == 0

    user = User.objects.create_superuser(email=email, password=password)

    assert User.objects.count() == 1
    assert user.email == email
    assert user.is_superuser == True
