import pytest
from django.contrib.auth import get_user_model
from django.core import mail
from django.urls import reverse_lazy
from rest_framework.status import HTTP_201_CREATED
from rest_framework.test import APIClient


@pytest.mark.django_db
def test_verification_email_is_sent_when_user_registers():
    user_registration_url = reverse_lazy("user_register")
    credentials = {
        "email": "user1@mail.com",
        "password": "password",
        "user_type": "consumer",
    }

    response = APIClient().post(user_registration_url, data=credentials)
    assert response.status_code == HTTP_201_CREATED

    User = get_user_model()
    assert User.objects.count() == 1
    assert len(mail.outbox) == 1
    assert mail.outbox[0].subject == "Salala account verification"
