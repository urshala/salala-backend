import pytest
from django.contrib.auth import get_user_model

from ..serializers import BaseUserSerializer


@pytest.mark.django_db
def test_can_register_with_valid_data():
    credentials = {
        "email": "user@mail.com",
        "password": "password",
        "user_type": "consumer",
    }
    serializer = BaseUserSerializer(data=credentials)
    serializer.is_valid(raise_exception=True)
    user = serializer.save()

    assert get_user_model().objects.count() == 1
    assert user.email == "user@mail.com"


@pytest.mark.django_db
@pytest.mark.parametrize(
    "email, password",
    [
        ("", ""),
        ("invalidemail", "password"),
        ("validemail@mail.com", ""),
        ("validemail@mail.com", "pass"),
    ],
)
def test_cannot_register_with_invalid_data(email, password):
    credentials = {"email": email, "password": password, "user_type": "consumer"}
    serializer = BaseUserSerializer(data=credentials)

    with pytest.raises(Exception) as e:
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
