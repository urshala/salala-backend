from django.urls import path

from .views import (
    ActivateAccountView,
    SalalaBaseUserRegistrationView,
    TokenObtainPairExtendedView,
    TokenRefreshExtendedView,
)

urlpatterns = [
    path("login/", TokenObtainPairExtendedView.as_view(), name="login"),
    path("token_refresh/", TokenRefreshExtendedView.as_view(), name="token_refresh"),
    path("register/", SalalaBaseUserRegistrationView.as_view(), name="user_register"),
    path(
        "activate/<uidb64>/<token>/",
        ActivateAccountView.as_view(),
        name="activate_user",
    ),
]
