from django.apps import AppConfig


class SalalaAuthConfig(AppConfig):
    name = "salala_auth"
