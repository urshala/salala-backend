from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .email import send_verification_email
from .serializers import BaseUserSerializer
from .token import account_activation_token


class TokenObtainPairExtendedView(TokenObtainPairView):
    pass


class TokenRefreshExtendedView(TokenRefreshView):
    pass


class SalalaBaseUserRegistrationView(CreateAPIView):
    serializer_class = BaseUserSerializer

    def perform_create(self, serializer):
        super().perform_create(serializer)
        self._send_verification_email(serializer.instance)

    def _send_verification_email(self, user):
        current_site = get_current_site(self.request)
        mail_context = {
            "user": user,
            "domain": current_site.domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": account_activation_token.make_token(user),
        }
        send_verification_email(user, mail_context)


class ActivateAccountView(APIView):
    def get(self, request, uidb64, token, *args, **kwargs):
        User = get_user_model()

        try:
            uuid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uuid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist) as e:
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_verified = True
            user.save()
            return Response({"success": "Successfully verified email!"})

        return Response({"failure": "Something went wrong!"})
