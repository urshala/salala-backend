
</html>
<body>
    <p>
        Hello, You have registered to salala. Please click the link provided with this email to verify yourself.
        Users who fail to verify can't use salala service.

        Verification link: https://{{ domain }}{% url 'activate_user' uidb64=uid token=token %}

    </p>

    <footer>
        Salala Oy
    </footer>
</body>
</html>
