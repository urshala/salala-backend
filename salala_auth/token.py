from django.contrib.auth.tokens import PasswordResetTokenGenerator


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    """
    This class is used to generate one time tokens, that will be used
    to check if the user is verified or not.
    """

    def _make_hash_value(self, user, timestamp):
        return str(user.pk) + str(timestamp) + str(user.is_verified)


account_activation_token = AccountActivationTokenGenerator()
